## Interface: 80000
## Title: Zug Zug Auto Role Setter
## Version: 8.0.0.0
## Author: Darool - Quel'Thalas
## Notes: Automatically sets your role when entering a party (battleground raid included) depending on your settings or spec

locale-deDE.lua
locale-enUS.lua
locale-esES.lua
locale-esMX.lua
locale-frFR.lua
locale-koKR.lua
locale-ruRU.lua
locale-zhCN.lua
locale-zhTW.lua

zzars.lua
