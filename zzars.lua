﻿zzars = LibStub("AceAddon-3.0"):NewAddon("zzars", "AceConsole-3.0", "AceEvent-3.0", "AceComm-3.0")

local AceConfig = LibStub("AceConfig-3.0");
local AceCongigDialog = LibStub("AceConfigDialog-3.0");

local zzars_version = "8.0.0.0";

local zzars_debug = "0";

local zzars_ActiveTalents = GetActiveTalentGroup(false, false);
local zzars_PrimaryTalentTree = GetPrimaryTalentTree(false, false, zzars_ActiveTalents);
local _, player = UnitClass("player");
local SetRole = CreateFrame("Frame");

local L = LibStub("AceLocale-3.0"):GetLocale("zzars")

-- RoleTypes
local ROLETYPES = {
	["1"] = "DPS",
	["2"] = "Tank",
	["3"] = "Healer",
}

zzars.defaults = {
        char = {
                handle = {
				["SpecBased"] = true,
                		["Preference"] = "1",
				["Debug"] = "0",
    		       },
		  },
}

zzars_Options = {
	name = L["zzars_config"],
	handler = zzars,
	type = 'group',
	args = {
		General = {
			order = 1,
			type = "group",
			name = L["zzars_general"],
			desc = L["zzars_generalsettings"],
			args = {
				SpecBased = {
					type = "toggle",
					order = 1,
					name = L["zzars_specbased_name"],
					desc = L["zzars_specbased_desc"],
					get = function(info) return zzars.db.char.handle["SpecBased"] end,
					set = function(info,input) zzars.db.char.handle["SpecBased"] = input end,
				},
				Preference = {
					type = "select",
					order = 2,
					values = ROLETYPES,
					name = L["zzars_preference_name"],
					desc = L["zzars_preference_desc"],
					get = function(info) return zzars.db.char.handle["Preference"] end,
					set = function(info,input) zzars.db.char.handle["Preference"] = input end,
				},
			},
		},
	},
}

function zzars:OnInitialize()
    -- Called when the addon is loaded
    AceConfig:RegisterOptionsTable("zzars.conf", zzars_Options, nil)
    self.optionsFrame = LibStub("AceConfigDialog-3.0"):AddToBlizOptions("zzars.conf", "Zug Zug Auto Role Setter")

    self.db = LibStub("AceDB-3.0"):New("zzars.DB", zzars.defaults)
    db = self.db.char
    LibStub("AceComm-3.0"):Embed(self)
end

function zzars:OnEnable()
	-- Called when the addon is enabled
	self:RegisterEvent("PLAYER_ENTERING_BATTLEGROUND")
	self:RegisterEvent("ACTIVE_TALENT_GROUP_CHANGED")
	self:RegisterEvent("PARTY_MEMBERS_CHANGED")
	self:RegisterEvent("PLAYER_TALENT_UPDATE")
	self:RegisterChatCommand("zzars", "SlashCmd")
	self:Print("ZugZug Auto Role Setter v"..zzars_version.." by Darool (Zug Zug @ Quel'Thalas)")
end

function zzars:PLAYER_ENTERING_BATTLEGROUND()
	if (zzars.db.char.handle["SpecBased"]) then
		-- SpecBased Is enabled, so set on spec
		self:SetRoleOnTalents()
	else
		-- Not specbased, set on preference
		self:SetRoleOnPreference()
	end
end
function zzars:ACTIVE_TALENT_GROUP_CHANGED()
	if (zzars.db.char.handle["SpecBased"]) then
		-- SpecBased Is enabled, so set on spec
		self:SetRoleOnTalents()
	else
		-- Not specbased, set on preference
		self:SetRoleOnPreference()
	end
end
function zzars:PARTY_MEMBERS_CHANGED()
	if (zzars.db.char.handle["SpecBased"]) then
		-- SpecBased Is enabled, so set on spec
		self:SetRoleOnTalents()
	else
		-- Not specbased, set on preference
		self:SetRoleOnPreference()
	end
end
function zzars:PLAYER_TALENT_UPDATE()
	if (zzars.db.char.handle["SpecBased"]) then
		-- SpecBased Is enabled, so set on spec
		self:SetRoleOnTalents()
	else
		-- Not specbased, set on preference
		self:SetRoleOnPreference()
	end
end

function zzars:SetRoleOnPreference()
	-- Get talents to block the roles he cannot do.
        zzars_ActiveTalents = GetActiveTalentGroup(false, false)
        zzars_PrimaryTalentTree = GetPrimaryTalentTree(false, false, zzars_ActiveTalents)
	zzars_canHeal = false
	zzars_canTank = false

	-- Only set it if player is above level 10
	if UnitLevel("player") >= 10 then
		-- Check if player has talents, else we cannot set role
		if zzars_PrimaryTalentTree == nil then
                        -- No talents, so no role
                        UnitSetRole("player", "No Role")
		else
			-- Talents, lets see if he can do it.
			if GetNumPartyMembers() > 0 or GetNumRaidMembers() > 0 then
				-- Ha in raid/group do it!
				if (player == "SHAMAN" and zzars_PrimaryTalentTree == 3) then
					-- Shaman, Healer
					zzars_canHeal = true
				end
				if (player == "PALADIN" and zzars_PrimaryTalentTree == 1) then
					-- Paladin, Healer
					zzars_canHeal = true
				end
				if (player == "PALADIN" and zzars_PrimaryTalentTree == 2) then
					-- Paladin, Tank
					zzars_canTank = true
				end
				if (player == "WARRIOR" and zzars_PrimaryTalentTree == 3) then
					-- Warrior, Tank
					zzars_canTank = true
				end
				if (player == "PRIEST" and zzars_PrimaryTalentTree <= 2) then
					-- Priest, Healer
					zzars_canHeal = true
				end
				if (player == "DEATHKNIGHT" and zzars_PrimaryTalentTree == 1) then
					-- DK, Tank
					zzars_canTank = true
				end

				-- We had the checks, now see what the user took!
				if (zzars.db.char.handle["Preference"] == "1") then
					UnitSetRole("player", "DAMAGER")
				elseif (zzars.db.char.handle["Preference"] == "2") then
					-- Tank
					if (zzars_canTank) then
						-- Its able to tank! YAY!
						UnitSetRole("player", "TANK")
					else
						self:ChatMSG("Cannot assign role TANK, due to talents!")
					end
				elseif (zzars.db.char.handle["Preference"] == "3") then
					-- Healer
					if (zzars_canHeal) then
						-- Its able to heal! yay!
						UnitSetRole("player", "HEALER")
					else
						self:ChatMSG("Cannot assign role Healer, due to talents!")
					end
				end
			end

		end
	end
end

function zzars:SetRoleOnTalents()
	-- Sets role based on talents
	zzars_ActiveTalents = GetActiveTalentGroup(false, false)
	zzars_PrimaryTalentTree = GetPrimaryTalentTree(false, false, zzars_ActiveTalents)

	if UnitLevel("player") >= 10 then
		-- Unit is above level 10, so is able to PVP
		if zzars_PrimaryTalentTree == nil then
			-- No talents, so no role
			UnitSetRole("player", "No Role")
		elseif zzars_PrimaryTalentTree ~= nil then
			-- We got talents, so we can decide a role
			if GetNumPartyMembers() > 0 or GetNumRaidMembers() > 0 then
				-- We are at least in a group/raid
				if (player == "MAGE" and zzars_PrimaryTalentTree >= 1) then
					-- Damager Class
					UnitSetRole("player", "DAMAGER")
				elseif (player == "HUNTER" and zzars_PrimaryTalentTree >= 1) then
					-- Damager Class
					UnitSetRole("player", "DAMAGER")
				elseif (player == "ROGUE" and zzars_PrimaryTalentTree >= 1) then
					-- Damager Class
					UnitSetRole("player", "DAMAGER")
				elseif (player == "SHAMAN" and zzars_PrimaryTalentTree <= 2) then
					-- Shaman, DPS
					UnitSetRole("player", "DAMAGER")
				elseif (player == "SHAMAN" and zzars_PrimaryTalentTree == 3) then
					-- Shaman, Healer
					UnitSetRole("player", "HEALER")
				elseif (player == "PALADIN" and zzars_PrimaryTalentTree == 1) then
					-- Paladin, Healer
					UnitSetRole("player", "HEALER")
				elseif (player == "PALADIN" and zzars_PrimaryTalentTree == 2) then
					-- Paladin, Tank
					UnitSetRole("player", "TANK")
				elseif (player == "PALADIN" and zzars_PrimaryTalentTree == 3) then
					-- Paladin, DPS
					UnitSetRole("player", "DAMAGER")
				elseif (player == "WARRIOR" and zzars_PrimaryTalentTree <= 2) then
					-- Warrior, DPS
					UnitSetRole("player", "DAMAGER")
				elseif (player == "WARRIOR" and zzars_PrimaryTalentTree == 3) then
					-- Warrior, TANK
					UnitSetRole("player", "TANK")
				elseif (player == "PRIEST" and zzars_PrimaryTalentTree <= 2) then
					-- Priest, Healer
					UnitSetRole("player", "HEALER")
				elseif (player == "PRIEST" and zzars_PrimaryTalentTree == 3) then
					-- Priest, DPS
					UnitSetRole("player", "DAMAGER")
				elseif (player == "DEATHKNIGHT" and zzars_PrimaryTalentTree == 1) then
					-- Deathknight, TANK
					UnitSetRole("player", "TANK")
				elseif (player == "DEATHKNIGHT" and zzars_PrimaryTalentTree >= 2) then
					-- Deathknight, DPS
					UnitSetRole("player", "DAMAGER")
				end
			end
		end
	end
end

function zzars:DebugMSG(msg)
	-- Display message
	if zzars.db.char.handle["Debug"] == "1" then
		self:Print("DEBUG: "..msg)
	end
end

function zzars:ChatMSG(msg)
	-- Display Message
	self:Print("ZZARS Warning: "..msg)
end

function zzars:ToggleDebug()
	if zzars.db.char.handle["Debug"] == "0" then
		zzars.db.char.handle["Debug"] = "1"
		self:Print("Debugging enabled")
	else
		zzars.db.char.handle["Debug"] = "0"
		self:Print("Debugging disabled")
	end
end

function zzars:SlashCmd(Cmd)
	if ( Cmd == "debug" ) then
		self:ToggleDebug()
	elseif ( Cmd == "help" ) then
		self:DisplayHelp()
	elseif ( Cmd == "about" ) then
		self:DisplayAbout()
	elseif ( Cmd == "reportprev" ) then
		self:ReportPrevTrack()
	else
		self:DisplayHelp()
	end
end

function zzars:DisplayAbout()
	-- About me and my addon
	self:Print("Zug Zug Huntar version "..zzars_version)
	self:Print("Created by Darool <Zug Zug> at Quel'Thalas-EU")
end

function zzars:DisplayHelp()
	-- Display Usage
	self:Print(L["zzars_usage"])
	self:Print(L["zzars_usage_help"])
	self:Print(L["zzars_usage_about"])
end

function zzars:GetMessage(info)
    return self.db.profile.message
end

function zzars:SetMessage(info, newValue)
    self.db.profile.message = newValue
end

function zzars:IsShowInChat(info)
    return self.db.profile.showInChat
end

function zzars:ToggleShowInChat(info, value)
    self.db.profile.showInChat = value
end

function zzars:IsShowOnScreen(info)
    return self.db.profile.showOnScreen
end

function zzars:ToggleShowOnScreen(info, value)
    self.db.profile.showOnScreen = value
end

function zzars:DebugLog(msg)
	return self.zzars_debug
end
